<html>
<head>
	<title>Seventh Station</title>
	<link href="//fonts.googleapis.com/css?family=Cinzel+Decorative:700|Heebo" rel="stylesheet">

	<link href="/css/common.css" rel="stylesheet">
	<link href="/css/header.css" rel="stylesheet">
	<link href="/css/main.css" rel="stylesheet">
	<link href="/css/modal.css" rel="stylesheet">
	<link href="/css/player.css" rel="stylesheet">
	<link href="/css/slider.css" rel="stylesheet">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="apple-touch-icon-precomposed" href="/img/iphone.png" />
	<link rel="icon" type="image/png" href="/img/favicon.png" />

	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

	<style type="text/css">
		.ytp-watermark {
			display: none !important;
		}

		.ytp-watermark:hover {
			display: none !important;
		}

		.yt-uix-sessionlink {
			display: none !important;
		}
	</style>
</head>
<body ng-app="app" ng-controller="controller">
	<div id="wrapper">

		<header>
			<nav id="top">
				<ul>
					<li ng-click="showModal(1); $event.stopPropagation();">Read</li>
					<li ng-click="showModal(2); $event.stopPropagation();">Listen</li>
					<li ng-click="showModal(3); $event.stopPropagation();">Watch</li>
					<li ng-click="showModal(4); $event.stopPropagation();">Contact</li>
				</ul>
			</nav>
			<div class="social-links">
				<a href="//www.facebook.com/SeventhStation" target="_blank"><i class="fa fa-facebook" title="Facebook"></i></a>
				<a href="//www.youtube.com/channel/UCFWGBgYA2_KuMm6DdCHY-ag" target="_blank"><i class="fa fa-youtube-play" title="YouTube"></i></a>
				<a href="//soundcloud.com/user-172443112" target="_blank"><i class="fa fa-soundcloud" title="SoundCloud"></i></a>
			</div>
		</header>

		<main ng-cloak>
			<div id="middle-stuff">
				<div id="logo">
					<img src="/img/logo.png" />
				</div>
				<div id="band-name">
					<div>Seventh</div>
					<div>Station</div>
				</div>
				<div id="show-player-btn" class="play-pause" ng-cloak>
					<i class="fa fa-play" aria-hidden="true"></i>
				</div>
				<!-- <div>
					<button id="myStopClickButton">Stop Music</button>
				</div> -->
			</div>
		</main>

		<div id="player">
			<div id="all-except-close">
				<div id="controls">
					<div id="mute-btn" ng-cloak>
						<i class="fa fa-volume-up" aria-hidden="true" style="cursor: pointer;"></i>
						<i class="fa fa-volume-down" aria-hidden="true" style="display: none; cursor: pointer;"></i>
						<i class="fa fa-volume-off" aria-hidden="true" style="display: none;"></i>
					</div>
					<input id="volume-slider" type="range" min="0" max="100" value="100" step="1" />
					<div style="margin: 0 100px; position: relative;">
						<input id="seek-slider" type="range" min="0" max="100" value="0" step="1" />
						<div id="timebox">
							<span id="cur-time-text">00:00</span>
							<span id="dur-time-text">09:06</span>
						</div>
					</div>

					<div id="play-pause-btn" class="play-pause" ng-cloak>
						<i class="fa fa-play" aria-hidden="true" style="display: none;"></i>
						<i class="fa fa-pause" aria-hidden="true"></i>
					</div>
				</div>

				<div id="song-name">Seventh Station &ndash; Abnormal Circles Doubtful Hour</div>
			</div>
			<div id="hide-player-btn" class="close">&times;</div>
		</div>
	</div>


	<!-- Modal -->

	<div id="modal" class="modal-wrapper">
		<div class="modal" ng-click="$event.stopPropagation();">

			<div class="close" ng-click="hideModal()">&times;</div>

			<div id="tabs">
				<div class="tab" ng-click="selectedItem = 1" ng-class="{selected: selectedItem == 1}" style="border-top-left-radius: 12px;">Read</div>
				<div class="tab" ng-click="selectedItem = 2" ng-class="{selected: selectedItem == 2}">Listen</div>
				<div class="tab" ng-click="selectedItem = 3" ng-class="{selected: selectedItem == 3}">Watch</div>
				<div class="tab" ng-click="selectedItem = 4" ng-class="{selected: selectedItem == 4}">Contact</div>
			</div>
			
			<div class="content">
				<div id="tab1" ng-if="selectedItem == 1">
					<h3>Biography</h3>
					<p>The idea of <b>Seventh Station</b> sprouted inside Dmitri's head back in 2008. The vision and the will to make the band alive became that strong that eventually led to the current lineup and release of the debut album <b>Between Life and Dreams.</b> The album has seen the light in September, 2016.</p>
					<p>The lineup is a result of Dmitri's strong focus in search for suitable band members. He came across keyboardist Eren Basbug &ndash; who is known for his work with Dream Theater, Jordan Rudess, etc. Later on vocalist Davidavi Dolev (Omb, Reign of the Architect, Gunned Down Horses, etc.) joined and after the release of the debut album drummer Panos Geo and bassist Laurant Da Pra filled the missing gaps.</p>
					<p>The band's immediate connection had led them to their first Slovenian tour and now the band is preparing for  another tour across Europe.</p>
					<h3>About the album</h3>
					<p><b>Between Life and Dreams</b> is no doubt a result of an ongoing strong will and wish to give to the listener the need for exploration. In these 65 minutes of devoted progressive metal the listener goes on a musical journey filled with varied musical and lyrical themes that combine in one. The album was produced by Dmitri Alperovich, engineered and mixed by Yonatan Kossov (Orphaned Land, Hummercult, Betzefer, etc.) and was recorded in Israel and United States. Album's artwork was done by painter Gregor Smukovič. The album received good reviews and its live presentation successfully drew the audience in.</p>
					<h3>Review quotes</h3>
					<blockquote>This is a combination of old and new influences, including those from musicians that are classically trained. This results in music that has a lot going on, with so many different styles involved that it would be pointless to try and list them all. (Wonderboxmetal.com).</blockquote>
					<blockquote>Many of their creations are inspired from classical music compositions in which they mix their own virtuoso inputs of modern and progressive metal. Believe me, the album will open your ears fully and lead you into the magical world of fantasy between two worlds, the reality and the dreams. (Paranoid webzine).</blockquote>
				
					<h3>Current Lineup</h3>

					<p>Davidavi (Vidi) Dolev &ndash; vocals<br />
					Dmitri Alperovich &ndash; guitars<br />
					Laurant Da Pra &ndash; bass guitar<br />
					Eren Basbug &ndash; keyboards<br />
					Panos Geo &ndash; drums</p>

					<h3>Album Credits</h3>

					<p>Produced by Dmitri Alperovich</p>

					<p>Davidavi (Vidi) Dolev &ndash; vocals<br />
					Dmitri Alperovich &ndash; guitars<br />
					Alexey Polyansky &ndash; bass guitar<br />
					Eren Basbug &ndash; keyboards, cboard<br />
					Dor Levin &ndash; drums, percussion, bouzouki</p>

					<p>Engineered and mixed by Yonatan Kossov at Bardo Studios, Ramat Gan, Israel<br />
					and Jaffa Sound Arts, Tel Aviv-Yafo, Israel<br />
					Addition Recordings at Eren Basbug’s home studio, Boston MA, USA<br />
					Assistant Engineering: Ofri Tamim<br />
					Additional Editing and Sound Effects: Dmitri Alperovich<br />
					Mastered by Michael Fossenkemper at TurtleTone Studio New York, USA</p>

					<p>Choir Arrangements by Alexey Polyansky<br />
					Choir Personnel: Ori Batchko, Boris Bendikov, Adi Bitran, Gennady Birenberg, Davidavi (Vidi) Dolev,<br />
					Eugene Kaminsky, Yoav Kaufman, Leah Marcu, Anna Senderovych and Rona Shrira<br />
					Narrations by Ori Batchko and Davidavi (Vidi) Dolev</p>

					<p>Art direction by Gregor Smukovic and Dmitri Alperovich<br />
					Cover Illustrations: Gregor Smukovic<br />
					Booklet Graphic Design: Gennady Birenberg<br />
					Logo by Alexandra Gichka</p>
				</div>
				<div id="tab2" ng-if="selectedItem == 2" style="display: flex; flex-direction: column; align-items: center; justify-content: space-between; height: 100%;">
					<iframe style="border: 0; width: 350px; height: 720px;" src="https://bandcamp.com/EmbeddedPlayer/album=3183613809/size=large/bgcol=333333/linkcol=0f91ff/transparent=true/" seamless><a href="http://seventhstation.bandcamp.com/album/between-life-and-dreams">Between Life and Dreams by Seventh Station</a></iframe>
				</div>
				<div id="tab3" ng-if="selectedItem == 3" style="display: flex; flex-direction: column; align-items: center; justify-content: space-between; height: 100%;">
					
					<div class="social-links" style="margin-bottom: 3em;">
						<a href="//www.facebook.com/SeventhStation" target="_blank"><i class="fa fa-facebook" title="Facebook"></i></a>
						<a href="//www.youtube.com/channel/UCFWGBgYA2_KuMm6DdCHY-ag" target="_blank"><i class="fa fa-youtube-play" title="YouTube"></i></a>
						<a href="//soundcloud.com/user-172443112" target="_blank"><i class="fa fa-soundcloud" title="SoundCloud"></i></a>
					</div>

					<div style="width:100%">
						<div>
							<h3>Seventh Station Live in Slovenia (April 2017)</h3>
							<div class="videoWrapper" style="margin-bottom: 3em;">
								<iframe width="1280" height="720" src="https://www.youtube.com/embed/3v0UxVBFaE8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
							</div>
							
							<h3>The Edge of the Lake</h3>
							<div class="videoWrapper" style="margin-bottom: 1em;">
								<iframe width="1280" height="720" src="https://www.youtube.com/embed/6fLWQ5kcYcI?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
							</div>
							<div style="font-size: 14px; margin-top: 10px; color: #333;">Shot in Slovenia February&ndash;May 2017. Music by Dmitri Alperovich and Alexey Polyansky. Lyrics by Dmitri Alperovich.</div>
						</div>
					</div>
				</div>
				<div id="tab4" ng-if="selectedItem == 4" style="display: flex; flex-direction: column; align-items: center; justify-content: center; height: 100%;">
					
					<h2>Management & Booking</h2>
					<p><a href="mailto:urska.strojin@gmail.com">urska.strojin@gmail.com</a></p>
					
					<div class="social-links" style="margin-top: 3em;">
						<a href="//www.facebook.com/SeventhStation" target="_blank"><i class="fa fa-facebook" title="Facebook"></i></a>
						<a href="//www.youtube.com/channel/UCFWGBgYA2_KuMm6DdCHY-ag" target="_blank"><i class="fa fa-youtube-play" title="YouTube"></i></a>
						<a href="//soundcloud.com/user-172443112" target="_blank"><i class="fa fa-soundcloud" title="SoundCloud"></i></a>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/controller.js"></script>
<script type="text/javascript" src="/js/player.js"></script>
<script type="text/javascript">
	$('#show-player-btn, #play-pause-btn').click(function(){
		$('iframe').each(function() {
      		var el_src = $(this).attr("src");
      		$(this).attr("src", el_src);
      	});
	});

	$('.videoWrapper').click(function(){
		alert('test');
	});
</script>
</html>