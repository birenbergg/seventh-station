(function(){

	angular.module("app").controller("controller", ['$scope', function ($scope) {

		//******  Modal  ******

		//When clicked outside, close modal
		$(document).click(function(event) {
			$scope.hideModal();
		});

		// When Escape pressed, close modal
		$(document).keypress(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		// Same as above, Chrome wrokaround
		$(document).keydown(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		$scope.showModal = function(id) {
			$scope.selectedItem = id;

			$('#modal').addClass('modal-open');
			$('header').addClass('header-when-modal-open');

			$('main').css('visibility', 'hidden');
			//$('header').css('visibility', 'hidden');
			$('#player').css('visibility', 'hidden');
		}

		$scope.hideModal = function() {
			$('#modal').removeClass('modal-open');
			$('header').removeClass('header-when-modal-open');

			$('main').css('visibility', 'visible');
			//$('header').css('visibility', 'visible');
			$('#player').css('visibility', 'visible');
		}
	}]);

}());