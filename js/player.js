var audio, player, showPlayerBtn, hidePlayerBtn, playPauseBtn, muteBtn, seekSlider, volumeSlider, curTimeText, durTimeText;

function initAudioPlayer() {
    audio = new Audio();
    audio.src = "/audio/acdh.mp3";

    // Set object references
    player = document.getElementById("player");

    showPlayerBtn = document.getElementById("show-player-btn");
    hidePlayerBtn = document.getElementById("hide-player-btn");

    playPauseBtn = document.getElementById("play-pause-btn");
    muteBtn = document.getElementById("mute-btn");

    seekSlider = document.getElementById("seek-slider");
    volumeSlider = document.getElementById("volume-slider");

    curTimeText = document.getElementById("cur-time-text");
    durTimeText = document.getElementById("dur-time-text");
    
    // Add Event Handling
    showPlayerBtn.addEventListener("click", showPlayer);
    hidePlayerBtn.addEventListener("click", hidePlayer);

    playPauseBtn.addEventListener("click", playPause);
    muteBtn.addEventListener("click", mute);

    seekSlider.addEventListener("change", setPosition);
    volumeSlider.addEventListener("change", setVolume);
    
    audio.addEventListener("timeupdate", seekTimeUpdate);
    audio.addEventListener("ended", function () { 
            document.getElementsByClassName('fa-pause')[0].style.display = 'none';
            document.getElementsByClassName('fa-play')[1].style.display = 'inline-block';
            audio.currentTime = 0;
    });
    
    // Functions
    function showPlayer() {        
        audio.play();
        showPlayerBtn.style.visibility = 'hidden';
        player.style.display = 'flex';

        document.getElementsByClassName('fa-play')[1].style.display = 'none';
        document.getElementsByClassName('fa-pause')[0].style.display = 'inline-block';
    }

    function hidePlayer() {
        audio.pause();
        showPlayerBtn.style.visibility = 'visible';
        player.style.display = 'none';
    }

    function playPause() {        
        if(audio.paused) {
            audio.play();
            document.getElementsByClassName('fa-play')[1].style.display = 'none';
            document.getElementsByClassName('fa-pause')[0].style.display = 'inline-block';
        } else {
            audio.pause();
            document.getElementsByClassName('fa-pause')[0].style.display = 'none';
            document.getElementsByClassName('fa-play')[1].style.display = 'inline-block';
        }
    }

    function setPosition() {
        audio.currentTime = audio.duration * (seekSlider.value / 100);
    }

    function setVolume() {
        audio.volume = volumeSlider.value / 100;
        
        if (audio.volume > .5) {
            document.getElementsByClassName('fa-volume-up')[0].style.display = 'inline-block';
            document.getElementsByClassName('fa-volume-down')[0].style.display = 'none';
            document.getElementsByClassName('fa-volume-off')[0].style.display = 'none';
        } else if (audio.volume == 0) {
            document.getElementsByClassName('fa-volume-up')[0].style.display = 'none';
            document.getElementsByClassName('fa-volume-down')[0].style.display = 'none';
            document.getElementsByClassName('fa-volume-off')[0].style.display = 'inline-block';
        } else {
            document.getElementsByClassName('fa-volume-up')[0].style.display = 'none';
            document.getElementsByClassName('fa-volume-down')[0].style.display = 'inline-block';
            document.getElementsByClassName('fa-volume-off')[0].style.display = 'none';
        }
    }

    function mute() {
        audio.volume = 0;
        volumeSlider.value = 0;

        document.getElementsByClassName('fa-volume-up')[0].style.display = 'none';
        document.getElementsByClassName('fa-volume-down')[0].style.display = 'none';
        document.getElementsByClassName('fa-volume-off')[0].style.display = 'inline-block';

        // if (!audio.muted) {
        //     audio.muted = true;

        //     document.getElementsByClassName('fa-volume-up')[0].style.display = 'none';
        //     document.getElementsByClassName('fa-volume-down')[0].style.display = 'none';
        //     document.getElementsByClassName('fa-volume-off')[0].style.display = 'inline-block';
        // } else {
        //     audio.muted = false;

        //     if (audio.volume > .5) {
        //         document.getElementsByClassName('fa-volume-up')[0].style.display = 'inline-block';
        //         document.getElementsByClassName('fa-volume-down')[0].style.display = 'none';
        //         document.getElementsByClassName('fa-volume-off')[0].style.display = 'none';
        //     } else {
        //         document.getElementsByClassName('fa-volume-up')[0].style.display = 'none';
        //         document.getElementsByClassName('fa-volume-down')[0].style.display = 'inline-block';
        //         document.getElementsByClassName('fa-volume-off')[0].style.display = 'none';
        //     }
        // }
    }

    function seekTimeUpdate() {
        var nt = audio.currentTime * (100 / audio.duration);
        seekSlider.value = nt;
        var curmins = Math.floor(audio.currentTime / 60);
        var cursecs = Math.floor(audio.currentTime - curmins * 60);
        var durmins = Math.floor(audio.duration / 60);
        var dursecs = Math.floor(audio.duration - durmins * 60);
        if(cursecs < 10) { cursecs = "0" + cursecs; }
        if(dursecs < 10) { dursecs = "0" + dursecs; }
        if(curmins < 10) { curmins = "0" + curmins; }
        if(durmins < 10) { durmins = "0" + durmins; }
        curTimeText.innerHTML = curmins + ":" + cursecs;
        durTimeText.innerHTML = durmins + ":" + dursecs;
    }
}

window.addEventListener("load", initAudioPlayer);